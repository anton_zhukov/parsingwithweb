package com.dev.bankdeposit.parser;

import com.dev.bankdeposit.entity.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class BanksDOMParser implements Parser {

    private static final Logger LOG = Logger.getLogger(BanksDOMParser.class);

    private Set<Investment> investments;

    public BanksDOMParser(String banksXmlFileName) {
        buildSetStudents(banksXmlFileName);
    }

    public void buildSetStudents(String banksXmlFileName) {
        investments = new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document doc = docBuilder.parse(banksXmlFileName);
            Element root = doc.getDocumentElement();
            NodeList roubleDeposits = root.getElementsByTagName(ParsingConstants.ROUBLE_DEPOSIT);
            for (int i = 0; i < roubleDeposits.getLength(); i++) {
                Node depositNode = roubleDeposits.item(i);
                RoubleDeposit roubleDeposit = buildRoubleDeposit(depositNode);
                investments.add(roubleDeposit);
            }

            NodeList currencyDeposits = root.getElementsByTagName(ParsingConstants.CURRENCY_DEPOSIT);
            for (int i = 0; i < currencyDeposits.getLength(); i++) {
                Node depositNode = currencyDeposits.item(i);
                CurrencyDeposit currencyDeposit = buildCurrencyDeposit(depositNode);
                investments.add(currencyDeposit);
            }
        } catch (ParserConfigurationException e) {
            LOG.error("ParserConfigurationException: " + e);
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        } catch (SAXException e) {
            LOG.error("Parsing failure: " + e);
        }
    }

    private RoubleDeposit buildRoubleDeposit(Node node) {
        RoubleDeposit roubleDeposit = new RoubleDeposit();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) node;
            buildInvestment(eElement, roubleDeposit);
            String prolongationAbility = getElementTextContent(eElement, ParsingConstants.PROLONGATION_ABILITY);
            roubleDeposit.setProlongationAbility(Boolean.valueOf(prolongationAbility));
        }
        return roubleDeposit;
    }

    private CurrencyDeposit buildCurrencyDeposit(Node node) {
        CurrencyDeposit currencyDeposit = new CurrencyDeposit();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) node;
            buildInvestment(eElement, currencyDeposit);
            String replenishment = getElementTextContent(eElement, ParsingConstants.REPLENISHMENT);
            Element insuranceElement = (Element) eElement.getElementsByTagName(ParsingConstants.INSURANCE).item(0);
            String currencyInsurance = getElementTextContent(insuranceElement, ParsingConstants.CURRENCY);
            String amountInsurance = getElementTextContent(insuranceElement, ParsingConstants.AMOUNT);
            String periodToPay = getElementTextContent(insuranceElement, ParsingConstants.PERIOD_TO_PAY);
            Insurance insurance = new Insurance(currencyInsurance, Double.parseDouble(amountInsurance),
                    DurationDataParser.parseData(periodToPay));
            currencyDeposit.setReplenishment(Boolean.parseBoolean(replenishment))
                    .setInsurance(insurance);
        }
        return currencyDeposit;
    }

    private static Investment buildInvestment(Element eElement, Investment investment) {
        int accountId = Integer.parseInt(eElement.getAttribute(ParsingConstants.ACCOUNT_ID).substring(1));
        String name = getElementTextContent(eElement, ParsingConstants.NAME);
        String country = getElementTextContent(eElement, ParsingConstants.COUNTRY);
        String type = getElementTextContent(eElement, ParsingConstants.TYPE);
        String depositor = getElementTextContent(eElement, ParsingConstants.DEPOSITOR);

        Element amountOnDeposit = (Element) eElement.getElementsByTagName(ParsingConstants.AMOUNT_ON_DEPOSIT).item(0);
        String currency = getElementTextContent(amountOnDeposit, ParsingConstants.CURRENCY);
        String amount = getElementTextContent(amountOnDeposit, ParsingConstants.AMOUNT);
        AmountOnDeposit onDeposit = new AmountOnDeposit(currency, Double.parseDouble(amount));

        double profitability = Double.parseDouble(getElementTextContent(eElement, ParsingConstants.PROFITABILITY));
        String timeConstraints = getElementTextContent(eElement, ParsingConstants.TIME_CONSTRAINTS);

        investment.setAccountId(accountId)
                .setName(name)
                .setCountry(country)
                .setDepositor(depositor)
                .setAmountOnDeposit(onDeposit)
                .setDepositType(DepositTypes.fromString(type))
                .setProfitability(profitability)
                .setTimeConstraints(DurationDataParser.parseData(timeConstraints));

        return investment;
    }


    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = null;
        if (nList != null) {
            node = nList.item(0);
        }
        return node != null ? node.getTextContent() : null;
    }

    @Override
    public Set<Investment> getInvestments() {
        return investments;
    }
}
