package com.dev.bankdeposit.controller;


import com.dev.bankdeposit.command.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RequestHelper {
    private static RequestHelper instance = new RequestHelper();
    private static AtomicBoolean instanceCreated = new AtomicBoolean();
    private static Lock lock = new ReentrantLock();

    private HashMap<String, Command> commands = new HashMap<>();

    private RequestHelper() {
        commands.put("dom", new DomParseCommand());
        commands.put("sax", new SaxParseCommand());
        commands.put("stax", new StaxParseCommand());
    }

    public static RequestHelper getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new RequestHelper();
                    instanceCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("choice");
        return commands.get(action);
    }
}
