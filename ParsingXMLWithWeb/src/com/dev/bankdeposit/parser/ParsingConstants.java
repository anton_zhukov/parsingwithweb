package com.dev.bankdeposit.parser;

public class ParsingConstants {

    public static final String ROUBLE_DEPOSIT = "rouble-deposit";
    public static final String CURRENCY_DEPOSIT = "currency-deposit";
    public static final String BANKS = "banks";
    public static final String ACCOUNT_ID = "account-id";
    public static final String NAME = "name";
    public static final String COUNTRY = "country";
    public static final String TYPE = "type";
    public static final String DEPOSITOR = "depositor";
    public static final String AMOUNT_ON_DEPOSIT = "amount-on-deposit";
    public static final String CURRENCY = "currency";
    public static final String AMOUNT = "amount";
    public static final String PROFITABILITY = "profitability";
    public static final String TIME_CONSTRAINTS = "time-constraints";
    public static final String PROLONGATION_ABILITY = "prolongation-ability";
    public static final String REPLENISHMENT = "replenishment";
    public static final String INSURANCE = "insurance";
    public static final String PERIOD_TO_PAY = "period-to-pay";
}
