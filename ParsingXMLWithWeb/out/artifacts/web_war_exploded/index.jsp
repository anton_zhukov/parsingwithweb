<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main page</title>
</head>
<body style="background: url('/images/random-wallpapers-white-screen-background-wallpaper-31442.jpg')">

<h2 style="color: black">Parser options:</h2>

<form action="/parser" method="post">
    <label>

        <select name="choice" class="" style="">
            <option value="sax">SAX</option>
            <option value="dom">DOM</option>
            <option value="stax">StAX</option>
        </select>

    </label><br/>
    <input type="submit" value="Parse">
</form>

</body>
</html>