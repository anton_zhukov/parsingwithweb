<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>${message}</title>
    <link rel="stylesheet" href="/css/styles.css"/>
</head>
<body>

<h1 style="color: black">${message}</h1>
<a href="index.jsp">Back to main page</a>
<hr>

<c:forEach var="deposit" items="${result}">

    <c:choose>
        <c:when test="${deposit.amountOnDeposit.currency=='BYR'}">
            <b><c:out value="Rouble-deposit"/></b>
        </c:when>
        <c:otherwise>
            <b><c:out value="Currency-deposit"/></b>
        </c:otherwise>
    </c:choose><br/>
    <table>
        <tr>
            <th>Field name</th>
            <th>Value</th>
        </tr>

        <tr>
            <td><b><c:out value="Account Id"/></b></td>
            <td><c:out value="${deposit.accountId}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Bank name"/></b></td>
            <td><c:out value="${deposit.name}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Country of registration"/></b></td>
            <td><c:out value="${deposit.country}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Deposit type"/></b></td>
            <td><c:out value="${deposit.depositType}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Depositor"/></b></td>
            <td><c:out value="${deposit.depositor}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Amount on deposit:"/></b></td>
            <td></td>
        </tr>
        <tr>
            <td><em><b><c:out value="currency"/></b></em></td>
            <td><c:out value=" ${deposit.amountOnDeposit.currency}"/></td>
        </tr>
        <tr>
            <td><em><b><c:out value="amount"/></b></em></td>
            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${deposit.amountOnDeposit.amount}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Profitability"/></b></td>
            <td><c:out value="${deposit.profitability}%"/></td>
        </tr>

        <tr>
            <c:if test="${deposit.timeConstraints!=0}">
                <td><b><c:out value="Time-constraints"/></b></td>
                <td><c:out value="${deposit.timeConstraints} milliseconds"/></td>
            </c:if>
        </tr>

        <c:choose>
            <c:when test="${deposit.amountOnDeposit.currency=='BYR'}">
                <tr>
                    <td><b><c:out value="Prolongation ability"/></b></td>
                    <td><c:out value="${deposit.prolongationAbility}"/></td>
                </tr>
            </c:when>
            <c:otherwise>
                <tr>
                    <td><b><c:out value="Replenishment"/></b></td>
                    <td><c:out value="${deposit.replenishment}"/></td>
                </tr>

                <tr>
                    <td><b><c:out value="Insurance:"/></b></td>
                    <td></td>
                </tr>

                <tr>
                    <td><em><b><c:out value="currency"/></b></em></td>
                    <td><c:out value="${deposit.insurance.currency}"/></td>
                </tr>

                <tr>
                    <td><b><em><c:out value="amount"/></em></b></td>
                    <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${deposit.insurance.amount}"/></td>
                </tr>

                <tr>
                    <td><b><em><c:out value="period to pay"/></em></b></td>
                    <td><c:out value="${deposit.insurance.periodToPay} milliseconds"/></td>
                </tr>
            </c:otherwise>
        </c:choose>

    </table>
    <br/>
</c:forEach>

<a href="index.jsp">Back to main page</a>
</body>
</html>
