package com.dev.bankdeposit.command;


import com.dev.bankdeposit.constants.Attributes;
import com.dev.bankdeposit.constants.MessagesText;
import com.dev.bankdeposit.constants.Pages;
import com.dev.bankdeposit.controller.Controller;
import com.dev.bankdeposit.entity.Investment;
import com.dev.bankdeposit.holder.XmlPathHolder;
import com.dev.bankdeposit.validator.XmlValidator;
import com.dev.bankdeposit.parser.BanksSAXParser;
import com.dev.bankdeposit.parser.Parser;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class SaxParseCommand implements Command {

    private static final Logger LOG = Logger.getLogger(SaxParseCommand.class);
    public static final String MESSAGE_TEXT = "SAX parser";

    public String execute(HttpServletRequest request) {
        LOG.info("SAXParser command");

        XmlPathHolder holder = XmlPathHolder.getInstance();
        boolean xmlIsValid = XmlValidator.validateFile(holder.getXsdPath(), holder.getXmlPath());

        String page = Pages.RESULT_PAGE;

        if (xmlIsValid) {
            Parser parserSax = new BanksSAXParser(holder.getXmlPath());
            Set<Investment> investmentsSax = parserSax.getInvestments();
            request.setAttribute(Attributes.RESULT, investmentsSax);
            request.setAttribute(Attributes.MESSAGE, MESSAGE_TEXT);
        } else {
            LOG.error("SAXParser command: xml is not valid");
            request.setAttribute(Attributes.MESSAGE, MessagesText.XML_IS_NOT_VALID_MESSAGE);
            page = Pages.ERROR_PAGE;
        }
        return page;
    }
}

