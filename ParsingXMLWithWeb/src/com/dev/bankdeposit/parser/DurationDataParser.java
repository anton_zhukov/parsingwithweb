package com.dev.bankdeposit.parser;

import java.util.concurrent.TimeUnit;

public class DurationDataParser {

    private static final String REGEX_DURATION = "[A-Za-z]";

    private static final long DAYS_IN_YEAR = 365;
    private static final long DAYS_IN_MONTH = 30;
    private static final long YEAR_MILLIS = TimeUnit.DAYS.toMillis(DAYS_IN_YEAR);
    private static final long MONTH_MILLIS = TimeUnit.DAYS.toMillis(DAYS_IN_MONTH);

    public static long parseData(String line) {
        if (line == null) return 0;
        String[] strings = line.substring(1).split(REGEX_DURATION);
        long result = (Long.parseLong(strings[0]) * YEAR_MILLIS);
        if (strings.length == 2) {
            result += (Long.parseLong(strings[1]) * MONTH_MILLIS);
        }
        return result;
    }
}