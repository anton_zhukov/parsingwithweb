package com.dev.bankdeposit.controller;

import com.dev.bankdeposit.command.Command;
import com.dev.bankdeposit.constants.*;
import com.dev.bankdeposit.holder.XmlPathHolder;

import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;


public class Controller extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String log4jLocation = context.getRealPath(config.getInitParameter(
                InitParameters.LOG4J_PROPERTIES_LOCATION));
        PropertyConfigurator.configure(log4jLocation);

        XmlPathHolder pathHolder = XmlPathHolder.getInstance();
        pathHolder.setXmlPath(context.getRealPath(config.getInitParameter(InitParameters.XML_FILE_LOCATION)));
        pathHolder.setXsdPath(context.getRealPath(config.getInitParameter(InitParameters.XSD_FILE_LOCATION)));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException {

        RequestHelper requestHelper = RequestHelper.getInstance();
        Command command = requestHelper.getCommand(request);
        String page = command.execute(request);
        request.getRequestDispatcher(page).forward(request, response);
    }
}

