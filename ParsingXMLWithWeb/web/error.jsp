<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
    <link rel="stylesheet" href="/css/styles.css"/>
</head>
<body>
<h1 style="color: black">ERROR PAGE</h1>
<hr>
${message}<br/><br/>
<a href="index.jsp">Back to main page</a>
</body>
</html>
