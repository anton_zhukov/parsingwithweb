package com.dev.bankdeposit.parser;

import com.dev.bankdeposit.entity.Investment;

import java.util.Set;

public interface Parser {

    Set<Investment> getInvestments();

}