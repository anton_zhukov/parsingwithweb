package com.dev.bankdeposit.constants;


public class Pages {

    public static final String RESULT_PAGE = "/resultParsing.jsp";
    public static final String ERROR_PAGE = "/error.jsp";

}
