package com.dev.bankdeposit.entity;

public class Investment {

    private int accountId;
    private String name;
    private String country;
    private DepositTypes depositType;
    private String depositor;
    private AmountOnDeposit amountOnDeposit;
    private double profitability;
    private long timeConstraints;

    public int getAccountId() {
        return accountId;
    }

    public Investment setAccountId(int accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Investment setName(String name) {
        this.name = name;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Investment setCountry(String country) {
        this.country = country;
        return this;
    }

    public DepositTypes getDepositType() {
        return depositType;
    }

    public Investment setDepositType(DepositTypes depositType) {
        this.depositType = depositType;
        return this;
    }

    public String getDepositor() {
        return depositor;
    }

    public Investment setDepositor(String depositor) {
        this.depositor = depositor;
        return this;
    }

    public AmountOnDeposit getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public Investment setAmountOnDeposit(AmountOnDeposit amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
        return this;
    }

    public double getProfitability() {
        return profitability;
    }

    public Investment setProfitability(double profitability) {
        this.profitability = profitability;
        return this;
    }

    public Long getTimeConstraints() {
        return timeConstraints;
    }

    public Investment setTimeConstraints(Long timeConstraints) {
        this.timeConstraints = timeConstraints;
        return this;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("accountId=").append(accountId).append("\n")
                .append("name=").append(name).append("\n")
                .append("country=").append(country).append("\n")
                .append("depositType=").append(depositType).append("\n")
                .append("depositor=").append(depositor).append("\n")
                .append("amountOnDeposit:").append(amountOnDeposit).append("\n")
                .append("profitability=").append(profitability).append("%\n")
                .append("timeConstraints=").append(timeConstraints).append(" milliseconds\n");
        return builder.toString();
    }
}
