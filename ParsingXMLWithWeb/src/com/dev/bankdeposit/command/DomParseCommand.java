package com.dev.bankdeposit.command;


import com.dev.bankdeposit.constants.Attributes;
import com.dev.bankdeposit.constants.MessagesText;
import com.dev.bankdeposit.constants.Pages;
import com.dev.bankdeposit.controller.Controller;
import com.dev.bankdeposit.entity.Investment;
import com.dev.bankdeposit.holder.XmlPathHolder;
import com.dev.bankdeposit.validator.XmlValidator;
import com.dev.bankdeposit.parser.BanksDOMParser;
import com.dev.bankdeposit.parser.Parser;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class DomParseCommand implements Command {
    private static final Logger LOG = Logger.getLogger(DomParseCommand.class);

    public static final String MESSAGE_TEXT = "DOM parser";

    public String execute(HttpServletRequest request) {
        LOG.info("DOMParser command");

        XmlPathHolder holder = XmlPathHolder.getInstance();
        boolean xmlIsValid = XmlValidator.validateFile(holder.getXsdPath(), holder.getXmlPath());

        String page = Pages.RESULT_PAGE;

        if (xmlIsValid) {

            LOG.info("Your choice: DOMParser");
            Parser parserDom = new BanksDOMParser(holder.getXmlPath());
            Set<Investment> investmentsDom = parserDom.getInvestments();
            request.setAttribute(Attributes.RESULT, investmentsDom);
            request.setAttribute(Attributes.MESSAGE, MESSAGE_TEXT);

        } else {
            request.setAttribute(Attributes.MESSAGE, MessagesText.XML_IS_NOT_VALID_MESSAGE);
            page = Pages.ERROR_PAGE;
        }
        return page;
    }
}

