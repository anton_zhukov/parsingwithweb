package com.dev.bankdeposit.constants;


public class InitParameters {

    public static final String LOG4J_PROPERTIES_LOCATION = "log4j-properties-location";
    public static final String XML_FILE_LOCATION = "xml-location";
    public static final String XSD_FILE_LOCATION = "xsd-location";

}
