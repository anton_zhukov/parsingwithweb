package com.dev.bankdeposit.parser;

import com.dev.bankdeposit.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashSet;
import java.util.Set;

public class BanksSAXHandler extends DefaultHandler {

    private Set<Investment> investments;
    private String value;
    private RoubleDeposit roubleDeposit;
    private CurrencyDeposit currencyDeposit;
    private AmountOnDeposit amountOnDeposit;
    private Insurance insurance;
    private boolean isCurrencyDeposit = false;
    private boolean isInInsurance = false;

    public BanksSAXHandler() {
        investments = new HashSet<>();
    }

    @Override
    public void startElement(String uri, String localName, String elementName, Attributes attributes) throws SAXException {

        if (elementName.equalsIgnoreCase(ParsingConstants.ROUBLE_DEPOSIT)) {
            isCurrencyDeposit = false;
            roubleDeposit = new RoubleDeposit();
            roubleDeposit.setAccountId(Integer.parseInt(attributes.getValue(ParsingConstants.ACCOUNT_ID).substring(1)));
        } else if (elementName.equalsIgnoreCase(ParsingConstants.CURRENCY_DEPOSIT)) {
            isCurrencyDeposit = true;
            currencyDeposit = new CurrencyDeposit();
            currencyDeposit.setAccountId(Integer.parseInt(attributes.getValue(ParsingConstants.ACCOUNT_ID).substring(1)));
        } else if (elementName.equalsIgnoreCase(ParsingConstants.AMOUNT_ON_DEPOSIT)) {
            isInInsurance = false;
            amountOnDeposit = new AmountOnDeposit();
        } else if (elementName.equalsIgnoreCase(ParsingConstants.INSURANCE)) {
            isInInsurance = true;
            insurance = new Insurance();
        }
    }

    @Override
    public void endElement(String uri, String localName, String element) throws SAXException {

        if (!isCurrencyDeposit) {
            endElementInvestment(roubleDeposit, element);
            if (element.equalsIgnoreCase(ParsingConstants.CURRENCY)) {
                amountOnDeposit.setCurrency(value);
            } else if (element.equalsIgnoreCase(ParsingConstants.AMOUNT)) {
                amountOnDeposit.setAmount(Double.parseDouble(value));
            } else if (element.equalsIgnoreCase(ParsingConstants.PROLONGATION_ABILITY)) {
                roubleDeposit.setProlongationAbility(Boolean.parseBoolean(value));
            } else if (element.equals(ParsingConstants.ROUBLE_DEPOSIT)) {
                investments.add(roubleDeposit);
            }
        } else {
            endElementInvestment(currencyDeposit, element);
            if (element.equalsIgnoreCase(ParsingConstants.REPLENISHMENT)) {
                currencyDeposit.setReplenishment(Boolean.parseBoolean(value));
            } else if (element.equalsIgnoreCase(ParsingConstants.INSURANCE)) {
                currencyDeposit.setInsurance(insurance);
            } else if (element.equalsIgnoreCase(ParsingConstants.CURRENCY)) {
                if (isInInsurance) {
                    insurance.setCurrency(value);
                } else {
                    amountOnDeposit.setCurrency(value);
                }
            } else if (element.equalsIgnoreCase(ParsingConstants.AMOUNT)) {
                if (isInInsurance) {
                    insurance.setAmount(Double.parseDouble(value));
                } else {
                    amountOnDeposit.setAmount(Double.parseDouble(value));
                }
            } else if (element.equalsIgnoreCase(ParsingConstants.PERIOD_TO_PAY)) {
                insurance.setPeriodToPay(DurationDataParser.parseData(value));
            } else if (element.equals(ParsingConstants.CURRENCY_DEPOSIT)) {
                investments.add(currencyDeposit);
            }
        }
    }

    private void endElementInvestment(Investment investment, String element) {
        if (element.equalsIgnoreCase(ParsingConstants.NAME)) {
            investment.setName(value);
        } else if (element.equalsIgnoreCase(ParsingConstants.COUNTRY)) {
            investment.setCountry(value);
        } else if (element.equalsIgnoreCase(ParsingConstants.TYPE)) {
            investment.setDepositType(DepositTypes.fromString(value));
        } else if (element.equalsIgnoreCase(ParsingConstants.DEPOSITOR)) {
            investment.setDepositor(value);
        } else if (element.equalsIgnoreCase(ParsingConstants.AMOUNT_ON_DEPOSIT)) {
            investment.setAmountOnDeposit(amountOnDeposit);
        } else if (element.equalsIgnoreCase(ParsingConstants.PROFITABILITY)) {
            investment.setProfitability(Double.parseDouble(value));
        } else if (element.equalsIgnoreCase(ParsingConstants.TIME_CONSTRAINTS)) {
            investment.setTimeConstraints(DurationDataParser.parseData(value));
        }
    }

    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        value = new String(ac, i, j);
    }

    public Set<Investment> getInvestments() {
        return investments;
    }
}


