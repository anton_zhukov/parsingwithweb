package com.dev.bankdeposit.validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XmlValidator {

    private static final Logger LOG = Logger.getLogger(XmlValidator.class);
    private static final String LANGUAGE = XMLConstants.W3C_XML_SCHEMA_NS_URI;

    public static boolean validateFile(String schemaPath, String filePath) {
        SchemaFactory factory = SchemaFactory.newInstance(LANGUAGE);
        File schemaLocation = new File(schemaPath);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(filePath);
            validator.validate(source);
            LOG.info(filePath + " is valid.");
            return true;
        } catch (SAXException e) {
            LOG.error("function " + filePath + " is not valid because " + e.getMessage());
        } catch (IOException e) {
            LOG.error(filePath + " is not valid because " + e.getMessage());
        }
        return false;
    }
}